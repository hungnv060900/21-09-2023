package com.devcamp.voucherrestapi.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.voucherrestapi.models.Voucher;

public interface IVoucherResponsitory extends JpaRepository<Voucher,Long> {
    
}
