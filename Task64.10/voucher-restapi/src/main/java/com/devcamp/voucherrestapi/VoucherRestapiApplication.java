package com.devcamp.voucherrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VoucherRestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(VoucherRestapiApplication.class, args);
	}

}
