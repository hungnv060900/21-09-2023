package com.devcamp.voucherrestapi.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.voucherrestapi.models.Voucher;
import com.devcamp.voucherrestapi.responsitory.IVoucherResponsitory;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class VoucherController {
    @Autowired
    IVoucherResponsitory iVoucherResponsitory;

    // get all
    @GetMapping("/vouchers")
    public ResponseEntity<List<Voucher>> getAllVouchers() {
        try {
            List<Voucher> pVouchers = new ArrayList<Voucher>();
            iVoucherResponsitory.findAll().forEach(pVouchers::add);
            // TODO: Hãy code lấy danh sách voucher từ DB
            return new ResponseEntity<>(pVouchers, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // get by id
    @GetMapping("/vouchers/{id}")
    public ResponseEntity<Voucher> getCVoucherById(@PathVariable("id") long id) {
        // Todo: viết code lấy voucher theo id tại đây
        try {
            Optional<Voucher> voucherData = iVoucherResponsitory.findById(id);
            if (voucherData.isPresent()) {
                return new ResponseEntity<>(voucherData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    // creat new voucher
    @PostMapping("/vouchers")
    public ResponseEntity<Voucher> createCVoucher(@Valid @RequestBody Voucher pVouchers) {
        try {
            // TODO: Hãy viết code tạo voucher đưa lên DB
            pVouchers.setNgayTao(new Date());
            pVouchers.setNgayCapNhat(null);
            Voucher _vouchers = iVoucherResponsitory.save(pVouchers);
            return new ResponseEntity<>(_vouchers, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // update voucher
    @PutMapping("/vouchers/{id}")
    public ResponseEntity<Voucher> updateCVoucherById(@PathVariable("id") long id, @Valid @RequestBody Voucher pVouchers) {
        try {
            // TODO: Hãy viết code lấy voucher từ DB để UPDATE
            Optional<Voucher> voucherData = iVoucherResponsitory.findById(id);

            if (voucherData.isPresent()) {
                Voucher voucher = voucherData.get();
                voucher.setMaVoucher(pVouchers.getMaVoucher());
                voucher.setPhanTramGiamGia(pVouchers.getPhanTramGiamGia());
                voucher.setGhiChu(pVouchers.getGhiChu());
                voucher.setNgayCapNhat(new Date());

                return new ResponseEntity<>(iVoucherResponsitory.save(voucher), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    // delete a voucher
    @DeleteMapping("/vouchers/{id}")
    public ResponseEntity<Voucher> deleteCVoucherById(@PathVariable("id") long id) {

        try {
            // TODO: Hãy viết code xóa 1 voucher từ DB
            iVoucherResponsitory.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
    

}
