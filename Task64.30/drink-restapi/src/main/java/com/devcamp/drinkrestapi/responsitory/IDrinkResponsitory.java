package com.devcamp.drinkrestapi.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.drinkrestapi.models.CDrink;

public interface IDrinkResponsitory extends JpaRepository<CDrink,Long> {
    
}
