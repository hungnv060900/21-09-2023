package com.devcamp.drinkrestapi.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.drinkrestapi.models.CDrink;
import com.devcamp.drinkrestapi.responsitory.IDrinkResponsitory;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CDrinkController {
    @Autowired
    IDrinkResponsitory iDrinkResponsitory;

    // get all
    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllDrinks() {
        try {
            List<CDrink> pDrinks = new ArrayList<CDrink>();
            iDrinkResponsitory.findAll().forEach(pDrinks::add);
            // TODO: Hãy code lấy danh sách voucher từ DB
            return new ResponseEntity<>(pDrinks, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // get by id
    @GetMapping("/drinks/{id}")
    public ResponseEntity<CDrink> getCDrinkById(@PathVariable("id") long id) {
        // Todo: viết code lấy drink theo id tại đây
        try {
            Optional<CDrink> drinkData = iDrinkResponsitory.findById(id);
            if (drinkData.isPresent()) {
                return new ResponseEntity<>(drinkData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    // creat new drink
    @PostMapping("/drinks")
    public ResponseEntity<CDrink> createCDrink(@Valid @RequestBody CDrink pDrinks) {
        try {
            // TODO: Hãy viết code tạo drink đưa lên DB
            pDrinks.setNgayTao(new Date());
            pDrinks.setNgayCapNhat(null);
            CDrink _drinks = iDrinkResponsitory.save(pDrinks);
            return new ResponseEntity<>(_drinks, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // update drink
    @PutMapping("/drinks/{id}")
    public ResponseEntity<CDrink> updateCVoucherById(@PathVariable("id") long id, @Valid @RequestBody CDrink pDrinks) {
        try {
            // TODO: Hãy viết code lấy voucher từ DB để UPDATE
            Optional<CDrink> drinkData = iDrinkResponsitory.findById(id);

            if (drinkData.isPresent()) {
                CDrink drink = drinkData.get();
                drink.setMaNuocUong(pDrinks.getMaNuocUong());
                drink.setTenNuocUong(pDrinks.getTenNuocUong());
                drink.setDonGia(pDrinks.getDonGia());
                drink.setGhiChu(pDrinks.getGhiChu());
                drink.setNgayCapNhat(new Date());

                return new ResponseEntity<>(iDrinkResponsitory.save(drink), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
    // delete a drink
    @DeleteMapping("/drink/{id}")
    public ResponseEntity<CDrink> deleteCDrinkById(@PathVariable("id") long id) {

        try {
            // TODO: Hãy viết code xóa 1 voucher từ DB
            iDrinkResponsitory.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
