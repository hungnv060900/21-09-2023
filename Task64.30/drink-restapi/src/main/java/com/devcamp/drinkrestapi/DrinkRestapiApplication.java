package com.devcamp.drinkrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DrinkRestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DrinkRestapiApplication.class, args);
	}

}
